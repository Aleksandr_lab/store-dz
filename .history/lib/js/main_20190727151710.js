'use strict';
/*Creat random product*/
function Product( title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50))) {
    this.title = title;
    this.imageSrc = imageSrc;
    this.price = price;
}

const products = [];
const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));
generatorProduct(5);
// localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) :
// localStorage.setItem('products', JSON.stringify(products)); 
/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();