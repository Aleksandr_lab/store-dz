'use strict';
/*Creat random product*/
function Product( title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50))) {
    this.title = title;
    this.imageSrc = imageSrc;
    this.price = price;
}
generatorProduct(5);
const products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [];
const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));
localStorage.setItem('products', JSON.stringify(products)); 
/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();