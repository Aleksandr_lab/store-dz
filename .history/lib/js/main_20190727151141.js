'use strict';
/*Creat random product*/
function Product( title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (10000 - 100) + 100))) {
    this.title = title;
    this.imageSrc = imageSrc;
    this.price = price;
}
const products = [];
const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));
generatorProduct(5)
/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();