'use strict';
/*Creat random product*/
function Product( title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50))) {
    this.title = title;
    this.imageSrc = imageSrc;
    this.price = price;
}

const products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
    generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

const formCreatProduct = document.getElementById('creat-product');
formCreatProduct.addEventListener('submit', (e)=>{
    e.preventDefault();
    let emountProduct = formCreatProduct.querySelector('#emountProduct').value;

    generatorProduct(Number(emountProduct));
    localStorage.setItem('products', JSON.stringify(products));

})

/*Create Dom Product */
const render = data => {
    const tmplateProduct = document.querySelector('#tmpl-product'),
        renderProducst = document.querySelector('#products');
    renderProducst.innerHTML = '';    
    for (let i in data) {
        let product = data[i],
            countProduct = Number(i) + 1,
            clone = tmplateProduct.content.cloneNode(true),
            img = clone.querySelectorAll('.product__img img'),
            title = clone.querySelectorAll('.product__title'),
            price = clone.querySelectorAll('.product__price'),
            btnAddCart = clone.querySelectorAll('.btn-add-cart');
        img[0].src = product.imageSrc;
        img[0].alt = `${product.title} ${countProduct}`;
        title[0].innerHTML = `${product.title} ${countProduct}`;
        price[0].innerHTML = product.price;
        btnAddCart[0].setAttribute('data-id', countProduct)
        btnAddCart[0].addEventListener('click', (e) =>{
            localStorage.removeItem('products');
            render(data.filter((el, j) => i != j));
            localStorage.setItem('products', JSON.stringify(products));
        })
        renderProducst.appendChild(clone);
    }

}

render(products);
/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();