'use strict';
(()=>{
    /*Сreat random category*/
    const categories = ['Backpacks', 'Bags_and_suitcases', 'Table_lamps', 'Sneakers', 'Wrist_Watch', 'Caps', 'Accessories', 'Lighting', 'Hats'],
    getRandomCategory = argsCategory => argsCategory[Math.floor(Math.random() * 8)];
    
    /*Creat random product*/
    function Product(
        title = 'Example Product', 
        category = getRandomCategory(categories),
        imageSrc, 
        price = Math.floor((Math.random() * (10000 - 150) + 150)), 
        id,
        count=1,
        ) {
        imageSrc =`./lib/img/${category}.webp`
        this.title = title;
        this.imageSrc = imageSrc;
        this.price = price;
        this.count = count;
        this.category = category;
        this.id = id;
    }

    /*Products and  cartProduct*/

    let products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
        cartProduct = localStorage.getItem("productsCart") ? JSON.parse(localStorage.getItem("productsCart")) : [];
    const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

    /*Add custom Product */

    const formCreatProduct = document.getElementById('creat-product');
    formCreatProduct.addEventListener('submit', (e)=>{
        e.preventDefault();
        let emountProduct = formCreatProduct.querySelector('#emountProduct').value;
        generatorProduct(Number(emountProduct));
        localStorage.setItem('products', JSON.stringify(products));
        render(products);
        [...filterCategory.querySelectorAll('option:not(:first-child)')].map(el => el.remove())
        argsActiveCategory();
    });

    /*Add Cutegory*/
    const sortByProducts = document.querySelector('#sortProducts'),
        filterCategory = document.querySelector('#filterCategory'),
        argsActiveCategory = () =>{ 
        products.map(el => el.category).filter((item, i, args) => args.indexOf(item) == i)
        .map(el=>{
            let opt = document.createElement('option'),
                content = document.createTextNode(el.split('_').join(' '));
            opt.setAttribute("value", el);
            opt.appendChild(content);
            filterCategory.appendChild(opt)
        })};
    argsActiveCategory();
    const filterSortProductCategory = (cat, min, max, param ='id', desc = 'DESC') =>{
        return products.filter(el => {
            return cat ? el.category == cat : el;
        }).sort((a,b) => {
        return desc == 'DESC' ? a[param] - b[param]: b[param] - a[param];
        }).filter((el, i) => el.price > min && el.price < max);
    };
    const sortBy = (sort,valFilter) =>{
        let reng1 = numberPrice[0].value,
            reng2 = numberPrice[1].value;
        switch (sort) {
            case 'sortMin':
                render(filterSortProductCategory(valFilter, reng1, reng2,'price'))
                break;
            case 'sortMax':
                render(filterSortProductCategory(valFilter, reng1, reng2, 'price', 'ASC'))
                break;
            case 'ASC':
                render(filterSortProductCategory(valFilter, reng1, reng2, 'id', sort))
                break;
        
            default:
                render(filterSortProductCategory(valFilter, reng1, reng2,))
                break;
        }
    };
    filterCategory.addEventListener('change', (e)=>{
        e.target.value == 'restCat' ? sortBy(sortByProducts.value) : sortBy(sortByProducts.value, e.target.value);
    })
    
    /*Sort by product*/
    sortByProducts.addEventListener('change', (e) => {
        filterCategory.value == 'restCat' ? sortBy(e.target.value) : sortBy(e.target.value, filterCategory.value);
    })
    
    /*Sort product rang */

    const rangPrice = document.querySelectorAll('#sliderReng input[type="range"]'),
        numberPrice = document.querySelectorAll('#sliderReng input[type="number"]'),
        startValueRangNumb = (elem1, elem2) => {
            elem1.value = Math.min(...[...products].map(el => el.price));
            elem2.value = Math.max(...[...products].map(el => el.price));
        },
        minMaxRang = (argsElem, argsProduct) => {
            [...argsElem].map(el => {
                el.min = Math.min(...[...argsProduct].map(el => el.price));
                el.max = Math.max(...[...argsProduct].map(el => el.price));
            });
        };
       /*Reng slide */ 
    [...rangPrice].map(el => {
        el.addEventListener('change', () => {
            let reng1 = parseFloat(rangPrice[0].value),
                reng2 = parseFloat(rangPrice[1].value);
            if (reng1 > reng2) {
                [reng1, reng2] = [reng2, reng1];
            }
            numberPrice[0].value = reng1;
            numberPrice[1].value = reng2;
            filterCategory.value == 'restCat' ? sortBy(filterCategory.value) : sortBy(sortByProducts.value, filterCategory.value);
        });
    });
        /*Reng number */
    [...numberPrice].map(el => {
        el.addEventListener('change', () => {
            let numb1 = parseFloat(numberPrice[0].value),
                numb2 = parseFloat(numberPrice[1].value);
            if (numb1 > numb2) {
                let tmp = numb1;
                numberPrice[0].value = numb2;
                numberPrice[1].value = tmp;
            }
            rangPrice[0].value = numb2;
            rangPrice[1].value = numb1;
            filterCategory.value == 'restCat' ? sortBy(filterCategory.value) : sortBy(sortByProducts.value, filterCategory.value);
        });
    });
    
    minMaxRang(rangPrice, products);
    minMaxRang(numberPrice, products);
    startValueRangNumb(numberPrice[0], rangPrice[0]);
    startValueRangNumb(numberPrice[1], rangPrice[1]);
    //numberPrice[1].value = Math.max(...[...products].map(el => el.price));
    //rangPrice[1].value = Math.max(...[...products].map(el => el.price));

    /*Search  Product*/
    const searchElem = document.getElementById('search-product'),
        seachProduct = str => products.filter(obj => Object.values(obj).join(' ').search(str) > 1);
    searchElem.addEventListener('change', (e) => {
        render(seachProduct(e.target.value));
    })
        
    /*Function current total  count*/
    const currentTotalCount = (args, param) => args.map(el => el[param]).reduce((a, b) => a + b);

    const templateProduct = document.querySelector('#tmpl-product'),
        renderProducst = document.querySelector('#products'),
        cart = document.querySelector('#cart'),
        templateProductCart = document.querySelector('#tmpl-product-cart'),
        navCart = document.querySelector('.nav-cart a'),
        cardContainer = document.querySelector('.card__conteiner'),
        cardTotal = document.querySelector('#card__total-price'),
        cardTotalCount = document.querySelector('.card__total-count'),
        totalProducts = document.querySelector('#total__products');

    /*Cart navigatin*/
    navCart.addEventListener('click', (e)=>{
        e.preventDefault();
        cardContainer.classList.toggle('d-block');
    })
    document.querySelector('.site-main').addEventListener('click', () => {
        cardContainer.classList.remove('d-block');
    })

    /*Total Card */
    if (cartProduct.length){
        cardTotal.innerHTML = currentTotalCount(cartProduct, 'price'); 
        cardTotalCount.innerHTML = currentTotalCount(cartProduct, 'count');
    }
    
        
    /*Create Dom Product */
    const render = (data, template = templateProduct, htmlRender = renderProducst) => {
        htmlRender.innerHTML = '';  
        for (let i in data) {
            let product = data[i],
                currentProducrPrice = products[i].price,
                clone = template.content.cloneNode(true),
                img = clone.querySelector('.product__img img'),
                title = clone.querySelector('.product__title'),
                price = clone.querySelector('.product__price'),
                btnAddCart = clone.querySelector('.product__add-cart'),
                btnRemoveProduct = clone.querySelector('.product__remove'),
                counterProduct = clone.querySelector('.product__counter'),
                productCategory = clone.querySelector('.product__category');
            if (data == products){
                product.id = Number(i) + 1
            }
            img.src = product.imageSrc;
            img.alt = `${product.title} ${product.id}`;
            title.innerHTML = `${product.title} ${product.id}`;
            price.innerHTML = product.price;
            /*Product Category */
            if (productCategory) {
                productCategory.innerHTML = product.category.split('_').join(' ');
            }
            
            /*Add to cart */
            if (btnAddCart){
                btnAddCart.addEventListener('click', (e) => {
                    cardContainer.classList.remove('d-none')
                        if (cartProduct.length){
                            let found = cartProduct.some((el) => {
                                return el.id === product.id;
                            });
                            if(!found){
                                cartProduct.push(product)
                            }else{
                                cartProduct.filter((el,z,args) => {
                                    if(el.id == product.id){
                                        el.count++;
                                        el.price = currentProducrPrice * el.count;
                                    }
                                    return args;
                                })
                            }
                        }else{
                            product.count = 1
                            cartProduct.push(product)
                        }
                    
                    cart.innerHTML = "";
                    localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                    render(cartProduct, templateProductCart, cart);
                    cardTotal.innerHTML = currentTotalCount(cartProduct, 'price');
                    cardTotalCount.innerHTML = currentTotalCount(cartProduct, 'count');
                }) 
            }
            /*Remove one Product*/
            if (btnRemoveProduct){
                btnRemoveProduct.addEventListener('click', () =>{
                    
                    if(cartProduct[i].count != 0){
                        cartProduct[i].price = product.price - (product.price / cartProduct[i].count);
                        cartProduct[i].count--;
                    } 
                    if (cartProduct[i].count < 1){
                        cartProduct.splice(i, 1)
                        cart.innerHTML = "";
                        render(cartProduct, templateProductCart, cart);
                        localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                    }else{
                        cart.innerHTML = "";
                        render(cartProduct, templateProductCart, cart);
                        localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                    }
                    if (cartProduct.length) {
                        cardTotal.innerHTML = currentTotalCount(cartProduct, 'price');
                        cardTotalCount.innerHTML = currentTotalCount(cartProduct, 'count');
                    }else{
                        cardTotal.innerHTML = '0';
                        cardTotalCount.innerHTML = '';
                    }
                    
                });
            }
            /*Counter product*/
            if (counterProduct) {
                counterProduct.innerHTML = product.count;
            }
            htmlRender.appendChild(clone);
        }
        totalProducts.innerHTML = data.length;
        if (!data.length) {
            htmlRender.innerHTML = '<div class="col-12"><h2>Not Products...</h2></div>';
        }
    } 
    render(products)
    render(cartProduct, templateProductCart, cart);
    /*Remove All Product */
    const removeProducts = document.querySelector('#products-remove');
    removeProducts.addEventListener('click', () => {
        if (products.length) {
            const isAgreement = confirm('Are you sure you want to remove all products');
            if (isAgreement) {
                cartProduct = [];
                localStorage.removeItem('productsCart')
                cart.innerHTML = '';
                cardTotal.innerHTML = '';
                cardTotalCount.innerHTML = '';
                removeProducts.title = 'Not products'
                cardContainer.classList.add('d-none')
                cardContainer.classList.remove('d-block')
            }
        } else {
            removeProducts.title = 'Remove products'
        }
    });
})();

/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();



