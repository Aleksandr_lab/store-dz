'use strict';
/*Creat random product*/
function Product( title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50))) {
    this.title = title;
    this.imageSrc = imageSrc;
    this.price = price;
}

const products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
    generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

const formCreatProduct = document.getElementById('creat-product');
formCreatProduct.addEventListener('submit', (e)=>{
    e.preventDefault();
    let emountProduct = formCreatProduct.querySelector('#emountProduct').value;

    generatorProduct(Number(emountProduct));
    localStorage.setItem('products', JSON.stringify(products));
    render(products);
})

/*Create Dom Product */
const render = data => {
    console.log(data)
    const tmplateProduct = document.querySelector('#tmpl-product'),
        renderProducst = document.querySelector('#products');
    renderProducst.innerHTML = '';    
    for (let i in data) {
        let product = data[i],
            countProduct = Number(i) + 1,
            clone = tmplateProduct.content.cloneNode(true),
            img = clone.querySelector('.product__img img'),
            title = clone.querySelector('.product__title'),
            price = clone.querySelector('.product__price'),
            btnAddCart = clone.querySelector('.product__add-cart'),
            btnRemoveProduct = clone.querySelector('.product__remove');
        img.src = product.imageSrc;
        img.alt = `${product.title} ${countProduct}`;
        title.innerHTML = `${product.title} ${countProduct}`;
        price.innerHTML = product.price;
        btnRemoveProduct.setAttribute('data-id', countProduct)
        btnRemoveProduct.addEventListener('click', (e) =>{
            localStorage.removeItem('products');
            render(data.filter((el, j) => i != j));
            localStorage.setItem('products', JSON.stringify(data.filter((el, j) => i != j)));
        })
        renderProducst.appendChild(clone);
    }

}
render(products);


/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();