'use strict';
(()=>{
    /*Creat random product*/
    function Product(title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50)), count=1) {
        this.title = title;
        this.imageSrc = imageSrc;
        this.price = price;
        this.count = count;
    }

    let products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
        cartProduct = localStorage.getItem("productsCart") ? JSON.parse(localStorage.getItem("productsCart")) : [];
    const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

    const formCreatProduct = document.getElementById('creat-product');
    formCreatProduct.addEventListener('submit', (e)=>{
        e.preventDefault();
        let emountProduct = formCreatProduct.querySelector('#emountProduct').value;

        generatorProduct(Number(emountProduct));
        localStorage.setItem('products', JSON.stringify(products));
        render(products);
    })

    /*Function current total  count*/
    const currentTotalCount = (args, param) => args.map(el => el[param]).reduce((a, b) => a + b);


    /*Remove All Product */
    const removeProducts = document.querySelector('#products-remove');
    removeProducts.addEventListener('click', () => {
        if (products.length != 0) {
            const isAgreement = confirm('Are you sure you want to remove all products');
            if (isAgreement) {
                products = [];
                cartProduct = [];
                localStorage.clear();
                render(products);
                render(cartProduct);
                removeProducts.title = 'Not products'
            }
        } else {
            removeProducts.title = 'Remove products'
        }
    })
    const templateProduct = document.querySelector('#tmpl-product'),
        renderProducst = document.querySelector('#products'),
        cart = document.querySelector('#cart'),
        templateProductCart = document.querySelector('#tmpl-product-cart'),
        navCart = document.querySelector('.nav-cart a'),
        cardContainer = document.querySelector('.card__conteiner'),
        cardTotal = document.querySelector('#card__total-price'),
        cardTotalCount = document.querySelector('.card__total-count');
    /*Cart navigatin*/
    navCart.addEventListener('click', (e)=>{
        e.preventDefault();
        cardContainer.classList.toggle('d-block');
    })
    // document.querySelector('.site-main').addEventListener('click', () => {
    //     cardContainer.classList.remove('d-block');
    // })

    /*Total Card */
    if (cartProduct.length != 0 ){
        cardTotal.innerHTML = currentTotalCount(cartProduct, 'price'); 
        cardTotalCount.innerHTML = currentTotalCount(cartProduct, 'count');
    }
    
        
    /*Create Dom Product */
    const render = (data, template = templateProduct, htmlRender = renderProducst) => {
        htmlRender.innerHTML = '';  
        for (let i in data) {
            let product = data[i],
                countProduct = Number(i) + 1,
                clone = template.content.cloneNode(true),
                img = clone.querySelector('.product__img img'),
                title = clone.querySelector('.product__title'),
                price = clone.querySelector('.product__price'),
                btnAddCart = clone.querySelector('.product__add-cart'),
                btnRemoveProduct = clone.querySelector('.product__remove'),
                counterProduct = clone.querySelector('.product__counter');
            img.src = product.imageSrc;
            img.alt = `${product.title} ${product.id + 1}`;
            title.innerHTML = `${product.title} ${product.id ? product.id + 1 : countProduct}`;
            price.innerHTML = product.price;
            
            /*Add to cart */
            if (btnAddCart){
                btnAddCart.addEventListener('click', (e) => {
                        product.id = Number(i);
                        if (cartProduct.length != 0){
                            let found = cartProduct.some((el) => {
                                return el.id === product.id;
                            });
                            if(!found){
                                cartProduct.push(product)
                            }else{
                                cartProduct.filter((el,z,args) => {
                                    if(el.id == product.id){
                                        el.count++;
                                        console.log(product.price)
                                        console.log(el.count)
                                        el.price = Number(el.price + (product.price * el.count));
                                    }
                                    return args;
                                })
                            }
                            //cartProduct.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
                        }else{
                            product.count = 1
                            cartProduct.push(product)
                        }
                    
                    cart.innerHTML = "";
                    localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                    render(cartProduct, templateProductCart, cart);
                    cardTotal.innerHTML = currentTotalCount(cartProduct, 'price');
                    cardTotalCount.innerHTML = currentTotalCount(cartProduct, 'count');;
                }) 
            }
            /*Remove one Product*/
            if (btnRemoveProduct){
                btnRemoveProduct.addEventListener('click', () =>{
                    
                    if(cartProduct[i].count != 0){
                        cartProduct[i].price = product.price - (product.price / cartProduct[i].count);
                        cartProduct[i].count--;
                    } 
                    if (cartProduct[i].count < 1){
                        cartProduct.splice(i, 1)
                        cart.innerHTML = "";
                        render(cartProduct, templateProductCart, cart);
                        localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                    }else{
                        cart.innerHTML = "";
                        render(cartProduct, templateProductCart, cart);
                        localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                    }
                    if (cartProduct.length != 0) {
                        cardTotal.innerHTML = currentTotalCount(cartProduct, 'price');
                        cardTotalCount.innerHTML = currentTotalCount(cartProduct, 'count');
                    }else{
                        cardTotal.innerHTML = '0';
                        cardTotalCount.innerHTML = '';
                    }
                    
                });
            }
            /*Counter product*/
            if (counterProduct) {
                counterProduct.innerHTML = product.count;
            }
            htmlRender.appendChild(clone);
        }
    }   
    render(products)
    render(cartProduct, templateProductCart, cart);
})();

/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();
