'use strict';
/*Creat random product*/
function Product( title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50))) {
    this.title = title;
    this.imageSrc = imageSrc;
    this.price = price;
}

const products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
    generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

const formCreatProduct = document.getElementById('creat-product');
formCreatProduct.addEventListener('submit', (e)=>{
    e.preventDefault();
    let emountProduct = formCreatProduct.querySelector('#emountProduct').value;

    generatorProduct(Number(emountProduct));
    localStorage.setItem('products', JSON.stringify(products));

})

/*Create Dom Product */
const tmplateProduct = document.querySelector('#tmpl-product')
for (let i in products) {
    let product = products[i],
        clone = tmplateProduct.content.cloneNode(true),
        img = clone.querySelectorAll('.product__img img'),
        title = clone.querySelectorAll('.product__title'),
        price = clone.querySelectorAll('.product__price');
    img[0].src = product.imageSrc;
    img[0].alt = `${product.title} ${i+1}`;
    title[0].innerHTML = `${product.title} ${i}`;
    price[0].innerHTML = product.price;
    tmplateProduct.parentNode.appendChild(clone);
}

/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();