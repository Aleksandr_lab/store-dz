'use strict';
(()=>{
    /*Creat random product*/
    function Product(title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50))) {
        this.title = title;
        this.imageSrc = imageSrc;
        this.price = price;
    }

    let products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
        cartProduct = [];
    const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

    const formCreatProduct = document.getElementById('creat-product');
    formCreatProduct.addEventListener('submit', (e)=>{
        e.preventDefault();
        let emountProduct = formCreatProduct.querySelector('#emountProduct').value;

        generatorProduct(Number(emountProduct));
        localStorage.setItem('products', JSON.stringify(products));
        render(products);
    })

    /*Remove All Product */
    const removeProducts = document.querySelector('#products-remove');
    removeProducts.addEventListener('click', () => {
        if (products.length != 0) {
            const isAgreement = confirm('Are you sure you want to remove all products');
            if (isAgreement) {
                products = [];
                localStorage.removeItem('products');
                render(products);
                removeProducts.title = 'Not products'
            }
        } else {
            removeProducts.title = 'Remove products'
        }
    })
    /*Create Dom Product */
    const render = data => {
        const templateProduct = document.querySelector('#tmpl-product'),
            renderProducst = document.querySelector('#products');
        renderProducst.innerHTML = '';  

        const renderTemplateProduct = (args, template, htmlRender) =>{ 
            for (let i in args) {
                let product = args[i],
                    countProduct = Number(i) + 1,
                    clone = template.content.cloneNode(true),
                    img = clone.querySelector('.product__img img'),
                    title = clone.querySelector('.product__title'),
                    price = clone.querySelector('.product__price'),
                    btnAddCart = clone.querySelector('.product__add-cart'),
                    btnRemoveProduct = clone.querySelector('.product__remove');
                img.src = product.imageSrc;
                img.alt = `${product.title} ${countProduct}`;
                title.innerHTML = `${product.title} ${countProduct}`;
                price.innerHTML = product.price;
                /*Remove one Product*/
                if (btnRemoveProduct){
                    btnRemoveProduct.addEventListener('click', (e) =>{
                        const isAgreement = confirm('Are you sure you want to remove this product?');
                        if (isAgreement) {
                            localStorage.removeItem('products');
                            render(data.filter((el, j) => i != j));
                            localStorage.setItem('products', JSON.stringify(data.filter((el, j) => i != j)));
                        }
                    })
                }
                /*Add to cart */
                if (btnAddCart){
                    btnAddCart.addEventListener('click', (e) => {
                        const cart = document.querySelector('#cart'),
                            templateProductCart = document.querySelector('#tmpl-product-cart');
                            product.id = Number(i);
                        cartProduct.push(product)
                            if (cartProduct.length != 0){
                                // if(cartProduct[j].id !== product.id){
                                //     console.log(true)
                                //     cartProduct.push(product)
                                // }
                                cartProduct.map(el => console.log(el.id != product.id))
                            }
                            /*else{
                                cartProduct.push(product)
                            }*/
                                
                            console.log(cartProduct)
                            
                        //console.log()
                        //renderTemplateProduct([product], templateProductCart, cart)
                    })
                }
                
                htmlRender.appendChild(clone);
            }
        }
        renderTemplateProduct(data, templateProduct, renderProducst);
    }
    render(products);
})();

/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();
