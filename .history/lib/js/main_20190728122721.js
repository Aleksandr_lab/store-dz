'use strict';
(()=>{
    /*Creat random product*/
    function Product(title = 'Example Product', imageSrc = `./lib/img/product${Math.floor((Math.random() * (9 - 1) + 1))}.webp`, price = Math.floor((Math.random() * (1000 - 50) + 50)), count=0) {
        this.title = title;
        this.imageSrc = imageSrc;
        this.price = price;
        this.count = count;
    }

    let products = localStorage.getItem("products") ? JSON.parse(localStorage.getItem("products")) : [],
        cartProduct = localStorage.getItem("productsCart") ? JSON.parse(localStorage.getItem("productsCart")) : [];
    const generatorProduct = (quanti) => Array.from(new Array(quanti)).map(el => products.push(new Product()));

    const formCreatProduct = document.getElementById('creat-product');
    formCreatProduct.addEventListener('submit', (e)=>{
        e.preventDefault();
        let emountProduct = formCreatProduct.querySelector('#emountProduct').value;

        generatorProduct(Number(emountProduct));
        localStorage.setItem('products', JSON.stringify(products));
        render(products);
    })

    /*Remove All Product */
    const removeProducts = document.querySelector('#products-remove');
    removeProducts.addEventListener('click', () => {
        if (products.length != 0) {
            const isAgreement = confirm('Are you sure you want to remove all products');
            if (isAgreement) {
                products = [];
                localStorage.removeItem('products');
                render(products);
                removeProducts.title = 'Not products'
            }
        } else {
            removeProducts.title = 'Remove products'
        }
    })
    const templateProduct = document.querySelector('#tmpl-product'),
        renderProducst = document.querySelector('#products'),
        cart = document.querySelector('#cart'),
        templateProductCart = document.querySelector('#tmpl-product-cart'),
        navCart = document.querySelector('.nav-cart'),
        cardComtainer = document.querySelector('.card__conteiner');
    /*Cart navigatin*/
    navCart.addEventListener('click', ()=>{
        cardComtainer.classList.toggle('show')
    })
        
    /*Create Dom Product */
    const render = (data, template = templateProduct, htmlRender = renderProducst) => {
        htmlRender.innerHTML = '';  
        for (let i in data) {
            let product = data[i],
                countProduct = Number(i) + 1,
                clone = template.content.cloneNode(true),
                img = clone.querySelector('.product__img img'),
                title = clone.querySelector('.product__title'),
                price = clone.querySelector('.product__price'),
                btnAddCart = clone.querySelector('.product__add-cart'),
                btnRemoveProduct = clone.querySelector('.product__remove'),
                counterProduct = clone.querySelector('.product__counter');
            img.src = product.imageSrc;
            img.alt = `${product.title} ${countProduct}`;
            title.innerHTML = `${product.title} ${countProduct}`;
            price.innerHTML = product.count == 0 ? product.price * (product.count + 1) : product.price *product.count;
            
            /*Counter product*/
            if (counterProduct) {
                counterProduct.innerHTML = product.count;
            }
            /*Add to cart */
            if (btnAddCart){
                btnAddCart.addEventListener('click', (e) => {
                    localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                        product.id = Number(i);
                        if (cartProduct.length != 0){
                            let found = cartProduct.some((el) => {
                                return el.id === product.id;
                            });
                            !found ? cartProduct.push(product) : null  
                        }else{
                            cartProduct.push(product)
                        }
                    cart.innerHTML = "";
                    product.count++;
                    render(cartProduct, templateProductCart, cart);
                    localStorage.removeItem('productsCart');
                    localStorage.setItem('productsCart', JSON.stringify(cartProduct));
                }) 
            }
            /*Remove one Product*/
            if (btnRemoveProduct){
                btnRemoveProduct.addEventListener('click', (e) =>{
                    e.preventDefault()
                    if (product.count === 1){
                        cart.innerHTML = "";
                        render(data.filter((el, j) => i != j), templateProductCart, cart);
                        localStorage.removeItem('productsCart');
                        localStorage.setItem('productsCart', JSON.stringify(data.filter((el, j) => i != j)));
                    }else{
                        cart.innerHTML = "";
                        product.count--
                        render(data, templateProductCart, cart);
                        localStorage.removeItem('productsCart');
                        localStorage.setItem('productsCart', JSON.stringify(data));
                    }
                    
                });
            }
            htmlRender.appendChild(clone);
        }
    }   
    render(products)
    render(cartProduct, templateProductCart, cart);
})();

/*Data copy right*/
const copyDate = document.getElementById('dateCopy');
copyDate.innerHTML = new Date().getFullYear();
